package com.yonder.model;

public class Post {
	private String id;
	private String description;
	private String author;	
	
	public Post(String _id, String _description, String _author) {
		id = _id;
		description = _description;
		author = _author;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", description=" + description + ", author=" + author + "]";
	}
}
