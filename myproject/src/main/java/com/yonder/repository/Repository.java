package com.yonder.repository;

import java.util.ArrayList;

import com.yonder.model.Post;

public class Repository {
	private ArrayList<Post> posts = new ArrayList<Post>();
	
	public ArrayList<Post> getAllPost() {
		posts.add(new Post("1","descr1","author1"));
		posts.add(new Post("2","descr2","author2"));
		posts.add(new Post("3","descr3","author3"));
		posts.add(new Post("4","descr4","author4"));
		return posts;
	}
	
	public void addPost(Post post) {
		posts.add(post);
	}
	
	public Post getByID(String id) {
		for (Post post : posts) {
			if (post.getId().equals(id))
				return post;
		}
		return null;
	}
}
